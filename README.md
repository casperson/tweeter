# tweeter
-Post a tweet and search through Twitter user base for user suggestions.

-After cloning, do an npm install. You may need to run npm install webpack-dev-server -g in order to run the webpack server to see it locally. Then just run webpack-dev-server from the directory of your files.

-Since the assignment stated that I could use a third party library for authentication, I
found a library called Codebird that made authentication and making calls to the endpoints a breeze.

-I started out using Backbone with React, but as I used Codebird, there wasn't a large need with
this app to have a need for Backbone since Codebird was handling all the authentication and calls
to the api. So I just used React heavily for the methods and rendering.

-I used ES6 features, have everything popping up in a nice table using bootstrap for the css mainly.

-All in all, this was a lot of fun! I had never worked with React before, and after taking
a nice dive into it, I really like how it does things. And I'd never connected to the Twitter
API before, which was also pretty cool.

-Just with this small app, I was impressed by the features it was requiring and it speaks volumes
to me about the efforts Sprout Social has put forth to stay at the forefront of tech.

-I'm looking forward to contributing to the work that will go on at Sprout Social.
